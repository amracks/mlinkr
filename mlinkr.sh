#!/bin/sh

APIKEY="YOUR API KEY HERE"

MQUERY_URL="https://api.themoviedb.org/3/search/movie?include_adult=false&page=1"
OIFS=${IFS}
IFS=$'\n'

echo "<html><body>"
for m in `cat ${1}`
do
    year=`echo ${m} | sed 's/.*(\([12][0-9][0-9][0-9]\)).*/\1/'`
    movie=`echo ${m} | sed 's/\(.*\)([12][0-9][0-9][0-9]).*/\1/'`
    movieu=`echo ${movie} | sed 's/ /+/g'`
    id=`curl -s --request GET \
        --url "${MQUERY_URL}&year=${year}&api_key=${APIKEY}&query=${movieu}" \
        --data '{}' | sed 's/.*"id":\([0-9][0-9]*\).*/\1/'`
    echo "<a href=\"https://www.themoviedb.org/movie/${id}\" target=\"_blank\">${movie}</a><br>"
done

echo "</body></html>"

# Revert IFS setting
IFS=${OIFS}
