# Generate links to TMDB from text list of movies

## Input
Input is the movie title and the year in parenthesis. See the Sample.txt for samples

## Usage
`./mlinkr.sh List.txt`

## Output
The output is html to stdout, redirect to save. The links will open in new tabs.
